
import math
import os

def my_int(x):
    try:
        x = int(x)
    except Exception, e:
        x = int(x[:-1])
    return x

class BindingLeverage(object):
    """Calculate leverage given the structure, the sites, and NMA"""

    def __init__(self):
        self.results_dir =  "/Users/agoncear/env-allodb/allodb/NMA/"
        # super(BindingLeverage, self).__init__()
        # self.arg = arg


    def calcBLC(self, site_leverage_P, site_leverage_Q):
        N = len(site_leverage_P) - 1
        lambda_P = [site_leverage_P[i] for i in range(N)]
        lambda_Q = [site_leverage_Q[i] for i in range(N)]
        D_PQ = [0.0 for i in range(N)]

        for mode in range(N):
            D_PQ[mode] = lambda_P[mode] * lambda_Q[mode]
        return sum(D_PQ)


    def distance(self, v1, v2):
        s = 0.0
        for i in range(3):
            s += (v2[i] - v1[i])**2
        return math.sqrt(s)


    def calcBLCoord(self, site, modes):
        # print "COORD"
        # print site
        # print modes
        if len(site) < 2:
            raise Exception("Less than two CA coordinates are known for the site")

        L = [0.0 for m in range(len(modes))]
        n_pairs = 0.0
        for i, r_i in enumerate(site):
            # print "i", i, r_i
            for j, r_j in enumerate(site):
                if j <= i: continue
                # print "j", j, r_j
                n_pairs += 1.0
                d = self.distance(r_i, r_j)
                for m, mode in enumerate(modes):
                    # print r_i, mode
                    r1_i = [r_i[0] + mode[i][0], r_i[1] + mode[i][1], r_i[2] + mode[i][2]]
                    r1_j = [r_j[0] + mode[j][0], r_j[1] + mode[j][1], r_j[2] + mode[j][2]]

                    d1 = self.distance(r1_i, r1_j)
                    # print "d=",d, "d1", d1
                    L[m] += (d1-d)**2
        for m in range(len(modes)):
            # print m, L[m]
            L[m] /= n_pairs
        L_sum = sum(L)
        L.append(L_sum)
        return L

    
    def calcBLResn(self, pdb, asm, site, normal_modes):
        # print normal_modes
        pdb_coords = self.readCoordinates(pdb, asm)
        site_coords = []
        site_modes = [[] for m in range(len(normal_modes))]
        chain = site['chain']
        for r, resi in enumerate(site['residues']):
            key = (chain, resi)
            resi_coord = pdb_coords.get(key)
            if resi_coord == None:
                # raise Exception("calcBLResn did not find coordinate for resi", key)
                continue
            else:
                site_coords.append(resi_coord)

                for m, mode in enumerate(normal_modes):
                    key = (chain, resi)
                    ca_mode = mode.get(key)
                    if ca_mode == None:
                        raise Exception("calcBLResn did not find normal modes for resi", key)
                    else:
                        site_modes[m].append(ca_mode)
                        # site_modes[m][r] = ca_mode

        return self.calcBLCoord(site_coords, site_modes)


    def readCoordinates(self, pdb_code, asm = 1):
        pdb_filename = self.results_dir + pdb_code[1:3].lower() + "/" + pdb_code.lower() + "." + str(asm) + ".pdb"
        if not os.path.exists(pdb_filename):
            print "BL readCoordinates file not found ", pdb_filename
            return {}

        coords = {}
        with open(pdb_filename, 'r') as f:
            for line in f:
                if line.startswith("ATOM"):
                    pdb_atomn = line[13:16].strip() # 'CA'
                    if pdb_atomn == "CA":
                        pdb_atomi = int(line[4:12].strip())
                        pdb_resn = line[17:20].strip()
                        pdb_chain = line[21:22].strip()
                        pdb_resi = my_int(line[22:26].strip())
                        coord = [float(line[30+8*i:38+8*i]) for i in range(3)]
                        coords[(pdb_chain, pdb_resi)] = coord
        return coords


    # def writeBL(bl_filename):
    #     with open(bl_filename, 'w'):            
    #         result = "#chain\tresi\tresn\tBL_value\n"
    #         for line in pdb_structure.split("\n"):
    #             if line.startswith("ATOM"):
    #                 pdb_atomn = line[13:16].strip()
    #                 if pdb_atomn == "CA":
    #                     pdb_atomi = line[4:12].strip()
    #                     pdb_resn = line[17:20].strip()
    #                     pdb_chain = line[21:22].strip()
    #                     pdb_resi = line[22:26].strip()
    #                     if (pdb_chain, pdb_resi) in values:
    #                         value = values[(pdb_chain, pdb_resi)]
    #                         result += "%s\t%s\t%s\t%.2f\n" % (pdb_chain, pdb_resi, pdb_resn, value)
    #         return result
