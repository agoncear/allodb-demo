"""
Calculates NMA for Bioassemblies in the PDB
"""

import fnmatch
import itertools
import os
import errno
import gzip

from MMTK import Units, InfiniteUniverse
from MMTK.Proteins import Protein
from MMTK.ForceFields import CalphaForceField
from MMTK.FourierBasis import FourierBasis, estimateCutoff
from MMTK.NormalModes import EnergeticModes, VibrationalModes

from PDB import PDB

"""
agoncear@bergasal:~/net/databases/PDB$ rsync -rlpt -v -z rsync.wwpdb.org::ftp/data/structures/divided/pdb/ biounit/

"""

def my_int(x):
    try:
        x = int(x)
    except Exception, e:
        x = int(x[:-1])
    return x

class NMA:

    def __init__(self):
        # super(NMA, self).__init__()
        self.structures_dir = "/Users/agoncear/projects/biounit/PDB/"
        self.results_dir =  "/Users/agoncear/env-allodb/allodb/NMA/"
        self.ibis_dir = "/Users/agoncear/projects/biounit/ibisdown/"


    def mkdir_p(self, path):
        # print path
        try:
            os.makedirs(path)
        except OSError as exc: # Python >2.5
            if exc.errno == errno.EEXIST and os.path.isdir(path):
                pass
            else: raise


    def IBIS_exists(self, pdb_code):
        filename = self.ibis_dir + pdb_code[1:3].upper() + "/" + pdb_code.upper() + ".txt"
        return os.path.exists(filename)


    def runAll(self, start_from="00"):
        """
            if results file exists - skip it
        """
        matches = []
        for root, dirnames, filenames in os.walk(self.structures_dir):
            for filename in fnmatch.filter(filenames, '*.gz'):
                pdb_code = ""
                asm = 0
                print filename
                if filename.endswith("ent.gz"):
                    # pdb2a8s.ent.gz
                    pdb_code = filename[3:7].lower()
                else:
                    pdb_code, pdb_asm, _ = os.path.basename(filename).lower().split(".", 2)
                    asm = int(pdb_asm[3:])
                
                if pdb_code[1:3] < start_from:
                    continue
                
                input_gz_filename = root + "/" + filename
                input_filename = self.results_dir + pdb_code[1:3] + "/" + pdb_code + "." + str(asm) + ".pdb"
                results_filename = self.results_dir + pdb_code[1:3] + "/" + pdb_code + "." + str(asm) + ".nma"
                print results_filename
                # print root, filename, pdb_code, asm

                if not self.IBIS_exists(pdb_code):
                    print "PDB %s not in IBIS. skipping" % (pdb_code)
                    continue

                try:
                    with open(results_filename):
                        pass
                except:
                    self.mkdir_p(self.results_dir + pdb_code[1:3])

                    with gzip.open(input_gz_filename, 'r') as in_gz:
                        with open(input_filename, 'w') as out_pdb:
                            raw_pdb = in_gz.read()
                            pdb = PDB(raw_pdb)
                            clean_pdb = pdb.cleanUp()
                            out_pdb.write(clean_pdb)

                    results = self.run(input_filename)
                    if results is not None:
                        with open(results_filename, "w") as f:
                            f.write(results)
                # matches.append(pdb_code)
        # return matches

    
    def read_by_pdb(self, pdb_code, asm = 1):
        filename = self.results_dir + pdb_code[1:3].lower() + "/" + pdb_code.lower() + "." + str(asm) + ".nma"
        if os.path.exists(filename):
            return self.read(filename)
        else:
            return None


    def read(self, filename):
        N = []
        mode = {}
        with open(filename, 'r') as f:
            for line in f:
                line = line.strip()
                if len(line) == 0:
                    continue
                elif line.startswith("BEGIN MODE"):
                    m = int(line.split(" ", 2)[2])
                    mode = {}
                elif line.startswith("END"):
                    N.append(mode)
                    continue
                else:
                    res, coords = line.split("\t", 1)
                    x, y, z = [float(x) for x in coords.split("\t", 2)]
                    chain, resi = res.split(":", 1)
                    mode[(chain, my_int(resi))] = [x, y, z]
        return N


    def run(self, in_filename):

        # Read the names of the residues
        if not os.path.exists(in_filename):
            print "NMA PDB file not found ", in_filename
            raise

        chain_numbers = []
        residue_numbers = {}
        with open(in_filename, 'r') as f:
            for line in f:
                if line.startswith("ATOM"):
                    pdb_atomn = line[13:16].strip() # 'CA'
                    if pdb_atomn == "CA":
                        pdb_atomi = int(line[4:12].strip())
                        pdb_resn = line[17:20].strip()
                        pdb_chain = line[21:22].strip()
                        pdb_resi = my_int(line[22:26].strip())

                        if len(chain_numbers) < 1 or chain_numbers[-1] != pdb_chain:
                            chain_numbers.append(pdb_chain)
                        chain_id = len(chain_numbers) - 1

                        if chain_id not in residue_numbers.keys():
                            residue_numbers[chain_id] = []
                        residue_numbers[chain_id].append(pdb_resi)
                        residue_id = len(residue_numbers[chain_id]) - 1
                        # print pdb_chain, chain_id, pdb_resn, pdb_resi, residue_id

        try:
           # Construct system
            universe = InfiniteUniverse(CalphaForceField(2.5))
            universe.protein = Protein(in_filename, model='calpha')

            modes = []
            natoms = universe.numberOfAtoms()
            # print "num atoms", natoms
            if natoms > 5000:
                print "Large molecule, %s CA atoms. Skipping" % (natoms,)
                return

            # Find a reasonable basis set size and cutoff
            nbasis = max(100, universe.numberOfAtoms()/5)
            cutoff, nbasis = estimateCutoff(universe, nbasis)
            # print "Calculating %d low-frequency modes." % nbasis
            if cutoff is None:
                # print "# EnergeticModes"
                modes = EnergeticModes(universe) # 300.*Units.K
            else:
                # Do subspace mode calculation with Fourier basis
                subspace = FourierBasis(universe, cutoff)
                # print "# EnergeticModes with FourierBasis cutoff and nbasis ", cutoff, nbasis
                modes = EnergeticModes(universe, None, subspace, sparse=True)
        
        except Exception as e:
            print "Could not calculate NMA (check the PDB file) ", in_filename, e
            # raise
            return

        # print "CALCULATED!!"
        # number of low freq modes, 6 first modes are trivial
        N = 6 + 10
        normal_modes = ""
        for i in range(6, N):
            normal_modes += "BEGIN MODE %d\n" % ((i+1))
            # mode = self.sortResidues(universe.protein, modes[i])

            for (chain, chain_id, chain_name) in zip(universe.protein, range(len(chain_numbers)), chain_numbers):
                for (residue, resi) in zip(chain.residues(), residue_numbers[chain_id]):
                    d = modes[i][residue.peptide.C_alpha]
                    normal_modes += "%s:%s\t%.10f\t%.10f\t%.10f\n" % (chain_name, resi, d.x(), d.y(), d.z())
                    # print "%s:%s\t%.10f\t%.10f\t%.10f\n" % (chain_name, resi, d.x(), d.y(), d.z())

            # for d in mode:
            #     normal_modes += "%.10f %.10f %.10f\n" % (d[0], d[1], d[2])
            normal_modes += "END\n\n"
        return normal_modes


if __name__ == '__main__':
    nma = NMA()
    nma.runAll("00")


