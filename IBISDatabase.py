"""
IBIS database 

Working with release 6/11/13

http://www.ncbi.nlm.nih.gov/Structure/ibis/ibis.cgi
ftp://ftp.ncbi.nih.gov/pub/mmdb/ibis/
"""

import fnmatch
import itertools
import os

#from Site import Site

IBIS_PATH = "/Users/agoncear/projects/biounit/ibisdown/"

def my_int(x):
    try:
        x = int(x)
    except Exception, e:
        x = int(x[:-1])
    return x

class IBISDatabase:

    def __init__(self, cache_dir = "/tmp/"):
        self.cache_dir = cache_dir


    def listPDB(self):
        matches = []
        for root, dirnames, filenames in os.walk(IBIS_PATH):
            for filename in fnmatch.filter(filenames, '*.txt'):
                pdb_code = os.path.basename(filename).lower().split(".", 1)[0]
                # print pdb_code
                matches.append(pdb_code)
        return matches

    def loadData(self, pdb_code):
        filename = IBIS_PATH + pdb_code[1:3] + "/" + pdb_code.upper() + ".txt"
        # print filename
        sites = []
        with open(filename) as f:
            for i, line in enumerate(f):                
                if i == 0: continue

                (   Query,
                    Interaction_type,
                    Mmdb_Residue_No,
                    PDB_Residue_No,
                    Binding_Site_Residues,
                    Binding_Site_Consevation,
                    Avg_PercentID,
                    Singleton,
                    PISA_validation,
                    Biol_Chemical_validation,
                    Site_CDD_Annotation,
                    Interaction_Partner,
                    PDB_Evidence,
                    Is_Observed,
                    Ranking_Score,
                    Query_Domain) = line.split(":", 15)

                if Interaction_type == "LIG":
                    # print Query, Interaction_Partner, Site_CDD_Annotation, Binding_Site_Residues, PDB_Residue_No
                    chain = Query[4:5]
                    # residue_numbers = [my_int(resi) for resi in PDB_Residue_No.split()]
                    residue_numbers = map(my_int, PDB_Residue_No.split())
                    residues = [(x, y, z) for x, y, z in itertools.izip(itertools.cycle(chain), Binding_Site_Residues, residue_numbers)]
                    # print chain, Interaction_Partner, residue_numbers #, residues

                    s = {
                        'chain': chain,
                        'ligand': Interaction_Partner,
                        'residues': residue_numbers}

                    if Interaction_Partner.lower() in ('', 'glycerol', 'sulfuric acid'):
                        continue
                    sites.append(s)
        return sites

    def loadPDBData(self, pdb_code):
        filename = IBIS_PATH + pdb_code[1:3] + "/" + pdb_code.upper() + ".txt"
        # print filename
        partners = []
        with open(filename) as f:
            for i, line in enumerate(f):                
                if i == 0: continue

                (   Query,
                    Interaction_type,
                    Mmdb_Residue_No,
                    PDB_Residue_No,
                    Binding_Site_Residues,
                    Binding_Site_Consevation,
                    Avg_PercentID,
                    Singleton,
                    PISA_validation,
                    Biol_Chemical_validation,
                    Site_CDD_Annotation,
                    Interaction_Partner,
                    PDB_Evidence,
                    Is_Observed,
                    Ranking_Score,
                    Query_Domain) = line.split(":", 15)

                if Interaction_type in ("LIG", "PPI"):
                    evidence = PDB_Evidence[0:4].lower()
                    partners.append((Interaction_type, evidence, int(Is_Observed)))

        return partners


if __name__ == '__main__':
    ibis = IBISDatabase()
    # print "Total #PDB in IBIS ", len(ibis.listPDB())

    print ibis.loadData("101m")
    print
    print ibis.loadData("20gs")
