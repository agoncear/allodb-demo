
import IBISDatabase as IBIS

BIOUNIT_PATH = "/Users/agoncear/projects/biounit/"
ibis = IBIS.IBISDatabase()
ibis_pdb = ibis.listPDB()

entries_file = BIOUNIT_PATH + "derived_data/index/entries.idx"
pdb_year = {}
entries = []
with open(entries_file) as f:
    for i, line in enumerate(f):
        if i > 1:
            pdb, header, date, compound, source, authors, resolution, experiment = line.split("\t", 7)
            year = int(date.split("/", 2)[2])
            if year < 50:
                year += 2000
            else:
                year += 1900
            pdb = pdb.lower()
            xray = 0
            if experiment.startswith("X-RAY"):
                xray = 1
            in_ibis = 0
            if pdb in ibis_pdb:
                in_ibis = 1
            entries.append( (pdb, year, xray, in_ibis) )
            pdb_year[pdb] = year

print "pdb\tyear\txtal\tibis\tpyear\tptype\tpobserved\n"
for (pdb, year, xray, in_ibis) in entries:
    if in_ibis == 1:
        partners = ibis.loadPDBData(pdb)
        pyear_summary = {}
        for int_type, evidence, observed in partners:
            if evidence not in pdb_year:
                print "ERROR in IBIS: What is ", evidence
                continue
            pyear = pdb_year[evidence]
            if pyear not in pyear_summary:
                pyear_summary[pyear] = [0, 0]
            pyear_summary[pyear][observed] += 1

        for pyear, summary in pyear_summary.iteritems():
            print pdb, pyear, summary[0], summary[1]
        # print year, pyear
    # print "%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t" %
        # (pdb, year, xray,)

