import os
import fnmatch

from NMA import NMA
from PDB import PDB
from BL import BindingLeverage
from IBISDatabase import IBISDatabase

"""
PROBLEMS: 
    a biological assembly is not necessarily the structure which is referred to in IBIS,
    e.g. 1s00 has chains L, M, H and their identical chains R, S, T
    however, not in asm file (there are no RST chains there)
    Conclusion: download default pdb files and calculate NMA/dpq for them
"""


def analysis(pdb, asm=1):
    ibis = IBISDatabase()
    nma = NMA()
    bl = BindingLeverage()

    ibis_sites = ibis.loadData(pdb)

    if len(ibis_sites) > 0:
        # Compare sites
        J = 0.
        for i, s1 in enumerate(ibis_sites):
            s1 = set([(s1['chain'], x) for x in s1['residues']])            
            for j, s2 in enumerate(ibis_sites):
                s2 = set([(s2['chain'], x) for x in s2['residues']])
                if j == i: break
                J = float(len(s1 & s2)) / len(s1 | s2) # Jaccard
                # print "Jaccard", i, j, J

        modes = nma.read_by_pdb(pdb, asm)
        leverages = []
        sites = []
        for i, site in enumerate(ibis_sites):
            # print pdb, site,
            try:
                site_leverage = bl.calcBLResn(pdb, asm, site, modes)
                leverages.append(site_leverage)
                sites.append(site)
            except Exception as e:
                print "Skipping site due to problems in BL calculation %s" % site, e
            # print round(site_leverage[-1], 4)
        Dpq = []
        for i, p in enumerate(leverages):
            Dpq.append([])
            Dpq[i] = []
            for j, q in enumerate(leverages):
                d = bl.calcBLC(p, q) * 10000.0
                # print "%s %s %.8f" % (i, j, d)
                Dpq[i].append(d)
        return sites, leverages, Dpq

def runAll():
    results_dir =  "/Users/agoncear/env-allodb/allodb/NMA/"
    for root, dirnames, filenames in os.walk(results_dir):
        for filename in fnmatch.filter(filenames, '*.nma'):
            # print filename
            pdb_code, pdb_asm, _ = os.path.basename(filename).lower().split(".", 2)
            asm = int(pdb_asm)
            print "Processing", pdb_code, asm

            sites_fname = results_dir + pdb_code[1:3] + "/" + pdb_code + "." + str(asm) + ".sites"
            bl_fname = results_dir + pdb_code[1:3] + "/" + pdb_code + "." + str(asm) + ".bl"
            dpq_fname = results_dir + pdb_code[1:3] + "/" + pdb_code + "." + str(asm) + ".dpq"

            # print sites_fname
            # print bl_fname
            # print dpq_fname

            try:
                sites, leverages, dpq = analysis(pdb_code, asm)
            except Exception as e:
                print "There occurred a problem with %s, asm %s (%s)" % (pdb_code, asm, e)
                continue

            if len(sites) < 1:
                print "No sites"
                continue

            with open(sites_fname, 'w') as f:
                f.write("site\tligand\tchain\tresi\n")
                for i, site in enumerate(sites):
                    f.write("%s\t%s\t%s\t%s\n" % (i, site['ligand'], site['chain'], ",".join(map(str, site['residues']))))

            with open(bl_fname, 'w') as f:
                # f.write("site\tligand\tchain\tresi")
                f.write("site")
                for m in range(len(leverages[0]) - 1):
                    f.write("\tL%s" % (m,))
                f.write("\tLsum\n")
                for i, site in zip(range(len(sites)), sites):
                    # f.write("%s\t%s\t%s\t%s" %
                    #     (i, site['ligand'], site['chain'], site['residues']))
                    f.write("%s" % (i))
                    for m in range(len(leverages[i]) - 1):
                        f.write("\t%f" % (leverages[i][m],))
                    f.write("\t%f\n" % (leverages[i][-1],))


            max_value = max([max(dp) for dp in dpq])
            print "max value", max_value
            with open(dpq_fname, 'w') as f:
                for i, p in enumerate(sites):
                    norm_values = map(lambda x: x/max_value, dpq[i])
                    f.write("\t".join(map(str, norm_values)))
                    f.write("\n")


if __name__ == '__main__':
    # analysis('103m')
    runAll()
    