"""
Listing the precalculated structures
"""

import fnmatch
import itertools
import os

#from Site import Site

RESULTS_PATH = "NMA/"
OUTPUT_PATH = "html/pdb/"

def create_listing():
    with open("listing.tab", 'w') as f:
        f.write("pdb\tsubdir\tasm\n")
        for root, dirnames, filenames in os.walk(RESULTS_PATH):
            for filename in fnmatch.filter(filenames, '*.sites'):
                pdb, asm, _ = os.path.basename(filename).lower().split(".", 2)
                subdir = pdb[1:3]
                f.write("%s\t%s\t%s\n" % (pdb, subdir, asm))

                path = OUTPUT_PATH + subdir + "/"
                try:
                    os.makedirs(path) 
                except:
                    pass


if __name__ == '__main__':
    create_listing()
