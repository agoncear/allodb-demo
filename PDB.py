"""
PDB File parser

"""

import string
from collections import defaultdict
import copy


class PDB(object):

    def __init__(self, pdb):
        self.pdb = pdb
        # clean_pdb = self.cleanUp(pdb_str)
        # self.parse(clean_pdb)
        # return clean_pdb


    def parse(self, clean_pdb):
        p = {}
        p['nr_atoms'] = 0
        p['nr_het_atoms'] = 0
        p['title'] = ""
        p['chains'] = []
        p['all_chains'] = [] # with hetero atoms
        p['pdb'] = None
        p['header'] = None

        for line in clean_pdb.split("\n"):
            if line.startswith("HEADER") and len(line) >= 66:
                p['pdb'] = line[62:66]
                p['header'] = line[10:50].strip()

            if line.startswith("TITLE") and len(line) >= 12:
                p['title'] += line[10:].rstrip()

            if line.startswith("ATOM "):
                p['nr_atoms'] += 1

                if len(line) > 22:
                    chain = line[21]
                    if chain not in p['chains']:
                        p['chains'].append(chain)
                    if chain not in p['all_chains']:
                        p['all_chains'].append(chain)

            if line.startswith("HETATM"):
                p['nr_het_atoms'] += 1

                if len(line) > 22:
                    chain = line[21]
                    if chain not in p['all_chains']:
                        p['all_chains'].append(chain)

        p['nr_chains'] = len(p['chains'])
        p['nr_all_chains'] = len(p['all_chains'])
        return p


    def removeLetters(self, line):
        """ code from pdb-utils by M.J. Harms"""
        if line[16] in string.letters:
            line = "%s %s" % (line[:16],line[17:])
        if line[26] in string.letters:
            line = "%s %s" % (line[:26],line[27:])
        return line


    def renameChains(self, pdb):
        """
        Follow WWWPDB conventions:

        http://www.wwpdb.org/procedure.html#toc_4
        What is the maximum number of chain IDs in a file?

        Up to 62 chains can be included in the PDB entry.
        Upper case letters and numbers (0-9) should be used first for chain IDs.
        Lower case letters should be used only after all upper letters and numbers have been used.
        Symbols should never be used for chain IDs.
        """
        all_chain_names = map(chr, range(65, 91))
        all_chain_names.append(map(chr, range(48, 58)))
        all_chain_names.append(map(chr, range(97, 123)))

        filtered_pdb = ""
        remaining_chains = copy.copy(all_chain_names)
        # filter(lambda a: a != 2, x)
        used_chains = set()
        current_chain = None
        new_chain_name = None
        for line in pdb.split("\n"):
            if line.startswith("ATOM "):
                chain = ''
                if len(line) > 22:
                    chain = line[21]
                if chain not in all_chain_names:
                    chain = 'A'
                if current_chain != chain:
                    # print "Found chain", chain
                    if chain in remaining_chains:
                        # print "in remaining"
                        remaining_chains = filter(lambda c: c != chain, remaining_chains)
                        new_chain_name = None
                    else:
                        # print "not in remaining"
                        new_chain_name = remaining_chains[0]
                        # print "new chain name", new_chain_name
                        remaining_chains = filter(lambda c: c != new_chain_name, remaining_chains)
                    current_chain = chain
                if new_chain_name is not None:
                    line = line[:21] + new_chain_name + line[22:]
            filtered_pdb += line + "\n"

            if line.startswith("TER"):
                chain = None
                current_chain = None
                new_chain_name = None

        return filtered_pdb


    def cleanUp(self):
        """
        Remove waters
        Remove unneeded records
        Remove alternative conformations
        Take only the first model from the NMR file

        """
        pdb_str = self.pdb
        clean_pdb = ""

        residues_atoms = defaultdict(list)

        for line in pdb_str.split("\n"):
            if len(line) == 0: continue
            if line.startswith("COMPND"): continue
            if line.startswith("SOURCE"): continue
            if line.startswith("KEYWDS"): continue
            if line.startswith("EXPDTA"): continue
            if line.startswith("AUTHOR"): continue
            if line.startswith("JRNL"): continue
            if line.startswith("REMARK"): continue
            if line.startswith("DBREF"): continue
            if line.startswith("SEQRES"): continue
            if line.startswith("FORMUL"): continue
            if line.startswith("SHEET"): continue
            if line.startswith("HELIX"): continue
            if line.startswith("CISPEP"): continue
            if line.startswith("ORIGX"): continue
            if line.startswith("SCALE"): continue
            if line.startswith("MTRIX"): continue
            if line.startswith("MASTER"): continue
            if line.startswith("MODEL"): continue

            if line.startswith("HETATM"): 
                if line[17:20] == "HOH": continue

            if line.startswith("ATOM"):
                residue = line[21:26]
                atom = line[12:16] # line[6:11]
                if atom not in residues_atoms[residue]:
                    line = self.removeLetters(line)
                    residues_atoms[residue].append(atom)
                else:
                    continue
            if line.startswith("ENDMDL"): break
            clean_pdb += line + "\n"

        # clean_pdb = self.renameChains(clean_pdb)
        self.pdb = clean_pdb
        return clean_pdb


    def removeLigands(self):

        no_ligands = ""
        for line in self.pdb.split("\n"):
            if line.startswith("HETATM"):
                continue
            no_ligands += line + "\n"

        return no_ligands


    def filterChains(self, clean_pdb, chains):
        """
        Remove chains that are not specified
        """
        lines = clean_pdb.split("\n")
        filtered_lines = []
        for line in lines:
            if line.startswith("TER") or line.startswith("ATOM") or line.startswith("HETATM"):
                if line[21] in chains:
                    filtered_lines.append(line)
        return "\n".join(filtered_lines)


if __name__ == '__main__':
    pass
